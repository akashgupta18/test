function top10EconomicalBowlersIn2015(deliveries, matches) {
    const result = {}
    const ids = matches.filter(obj => obj['season'] === '2015').map(obj => parseInt(obj.id))
    const dels = deliveries.filter(del => ids.includes(parseInt(del['match_id'])))
    for (let id of ids) {
        for (let del of dels) {
            if (parseInt(del['match_id']) === id) {
                if (result[del.bowler]) {
                    result[del.bowler].runConceded += parseInt(del['total_runs'])
                    result[del.bowler].bowls += 1
                } else {
                    result[del.bowler] = {}
                    result[del.bowler].runConceded = parseInt(del['total_runs'])
                    result[del.bowler].bowls = 1
                }
            }
        }
    }

    let sortable = [];
    for (let key in result) {
        result[key].economuRate = +((result[key].runConceded / (result[key].bowls) / 6).toFixed(2));
        sortable.push([key, result[key].economuRate]);
    }

    sortable.sort(function(a, b) {
        return a[1] - b[1];
    });

    var top10EconomicalBowler = {};
    sortable.slice(0, 10).forEach(key => top10EconomicalBowler[key[0]] = key[1]);

    return top10EconomicalBowler;
}

module.exports = top10EconomicalBowlersIn2015;